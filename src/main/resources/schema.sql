create table users(
    user_id integer primary key not null auto_increment,
    first_name varchar(30) not null,
    last_name varchar(30) not null,
    email varchar(50) not null,
    password text not null
);

create table categories(
    category_id integer primary key not null auto_increment,
    user_id integer not null,
    title varchar(30) not null,
    description varchar(50) not null,
    total_expense double not null
);
alter table categories add constraint cat_user_fk foreign key (user_id) references users(user_id);

create table transactions(
    transaction_id integer primary key not null auto_increment,
    category_id integer not null,
    user_id integer not null,
    amount numeric(10,2) not null,
    note varchar(50) not null,
    transaction_date bigint not null
)
auto_increment=1000;
alter table transactions add constraint trans_cat_fk foreign key (category_id) references categories(category_id);
alter table transactions add constraint trans_users_fk foreign key (user_id) references users(user_id);
