package com.tuto.restapi.services;

import java.security.SecureRandom;

import com.tuto.restapi.entities.User;
import com.tuto.restapi.exceptions.ApiAuthException;
import com.tuto.restapi.repositories.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public void addUser(User user) {
        if (userRepository.existsByEmail(user.getEmail()))
            throw new ApiAuthException("Email already exists");
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(10, new SecureRandom());
        user.setPassword(encoder.encode(user.getPassword()));
        userRepository.save(user);
    }

    public User validateUser(String email, String password) {
        if (!userRepository.existsByEmail(email))
            throw new ApiAuthException("Invalid email/password");
        User user = userRepository.getByEmail(email);
        if (!new BCryptPasswordEncoder().matches(password, user.getPassword()))
            throw new ApiAuthException("Invalid email/password");
        return user;
    }
}
