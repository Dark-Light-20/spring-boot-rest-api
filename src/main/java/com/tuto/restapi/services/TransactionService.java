package com.tuto.restapi.services;

import java.util.List;

import javax.transaction.Transactional;

import com.tuto.restapi.entities.Transaction;
import com.tuto.restapi.exceptions.ApiBadRequestException;
import com.tuto.restapi.exceptions.ApiResourceNotFoundException;
import com.tuto.restapi.repositories.TransactionRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class TransactionService {

    @Autowired
    private TransactionRepository transactionRepository;

    public List<Transaction> getTransactionsByCategory(Integer userId, Integer categoryId) {
        return transactionRepository.getByUserIdAndCategoryId(userId, categoryId);
    }

    public Transaction getTransactionById(Integer userId, Integer categoryId, Integer transactionId)
            throws ApiResourceNotFoundException {
        try {
            return transactionRepository.getByUserIdAndCategoryIdAndTransactionId(userId, categoryId, transactionId);
        } catch (Exception e) {
            System.err.println(e);
            throw new ApiResourceNotFoundException("Transaction not found!");
        }
    }

    public Transaction addTransaction(Integer userId, Integer categoryId, Transaction transaction)
            throws ApiBadRequestException {
        transaction.setUserId(userId);
        transaction.setCategoryId(categoryId);
        return transactionRepository.save(transaction);
    }

    public Transaction updateTransaction(Integer userId, Integer categoryId, Integer transactionId,
            Transaction transaction) throws ApiBadRequestException {
        try {
            if (transactionRepository.existsByUserIdAndCategoryId(userId, categoryId)) {
                transaction.setUserId(userId);
                transaction.setCategoryId(categoryId);
                transaction.setTransactionId(transactionId);
                return transactionRepository.save(transaction);
            } else
                throw new ApiBadRequestException("Category not found!");
        } catch (Exception e) {
            System.err.println(e);
            throw new ApiBadRequestException("Invalid request");
        }
    }

    public void deleteTransaction(Integer userId, Integer categoryId, Integer transactionId)
            throws ApiResourceNotFoundException {
        getTransactionById(userId, categoryId, transactionId);
        transactionRepository.deleteByUserIdAndCategoryIdAndTransactionId(userId, categoryId, transactionId);
    }
}
