package com.tuto.restapi.services;

import java.util.List;

import com.tuto.restapi.entities.Category;
import com.tuto.restapi.exceptions.ApiBadRequestException;
import com.tuto.restapi.exceptions.ApiResourceNotFoundException;
import com.tuto.restapi.repositories.CategoryRepository;
import com.tuto.restapi.repositories.TransactionRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    public List<Category> getCategories(Integer userId) {
        return categoryRepository.getCategories(userId);
    }

    public Category getCategoryById(Integer userId, Integer categoryId) throws ApiResourceNotFoundException {
        try {
            return categoryRepository.getCategoryById(userId, categoryId);
        } catch (Exception e) {
            System.err.println(e);
            throw new ApiResourceNotFoundException("Category not found!");
        }
    }

    public Category addCategory(Integer userId, Category category) throws ApiBadRequestException {
        try {
            category.setUserId(userId);
            category.setTotalExpense(0.0);
            return categoryRepository.save(category);
        } catch (Exception e) {
            System.err.println(e);
            throw new ApiBadRequestException("Invalid request");
        }
    }

    public Category updateCategory(Integer userId, Integer categoryId, Category category)
            throws ApiBadRequestException {
        try {
            category.setUserId(userId);
            category.setCategoryId(categoryId);
            categoryRepository.save(category);
            return getCategoryById(userId, categoryId);
        } catch (Exception e) {
            System.err.println(e);
            throw new ApiBadRequestException("Invalid request");
        }
    }

    public void deleteCategoryWithAllTransactions(Integer userId, Integer categoryId)
            throws ApiResourceNotFoundException {
        getCategoryById(userId, categoryId);
        transactionRepository.deleteByUserIdAndCategoryId(userId, categoryId);
        categoryRepository.deleteById(categoryId);
    }
}
