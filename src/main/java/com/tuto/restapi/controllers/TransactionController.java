package com.tuto.restapi.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tuto.restapi.entities.Transaction;
import com.tuto.restapi.services.TransactionService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@CrossOrigin
@RequestMapping("/api/categories/{categoryId}/transactions")
public class TransactionController {

    @Autowired
    private TransactionService transactionService;

    @GetMapping
    public ResponseEntity<List<Transaction>> getTransactions(@RequestAttribute Integer userId,
            @PathVariable Integer categoryId) {
        return new ResponseEntity<>(transactionService.getTransactionsByCategory(userId, categoryId), HttpStatus.OK);
    }

    @GetMapping("{transactionId}")
    public ResponseEntity<Transaction> getTransactionById(@RequestAttribute Integer userId,
            @PathVariable Integer categoryId, @PathVariable Integer transactionId) {
        return new ResponseEntity<>(transactionService.getTransactionById(userId, categoryId, transactionId),
                HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Transaction> addTransaction(@RequestAttribute Integer userId,
            @PathVariable Integer categoryId, @RequestBody Transaction transaction) {
        return new ResponseEntity<>(transactionService.addTransaction(userId, categoryId, transaction),
                HttpStatus.CREATED);
    }

    @PutMapping("{transactionId}")
    public ResponseEntity<Transaction> updateTransaction(@RequestAttribute Integer userId,
            @PathVariable Integer categoryId, @PathVariable Integer transactionId,
            @RequestBody Transaction transaction) {
        return new ResponseEntity<>(
                transactionService.updateTransaction(userId, categoryId, transactionId, transaction), HttpStatus.OK);
    }

    @DeleteMapping("{transactionId}")
    public ResponseEntity<Map<String, Boolean>> deleteTransaction(@RequestAttribute Integer userId,
            @PathVariable Integer categoryId, @PathVariable Integer transactionId) {
        transactionService.deleteTransaction(userId, categoryId, transactionId);
        Map<String, Boolean> map = new HashMap<>();
        map.put("success", true);
        return new ResponseEntity<>(map, HttpStatus.OK);
    }
}
