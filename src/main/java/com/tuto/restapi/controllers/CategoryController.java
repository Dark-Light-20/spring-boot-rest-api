package com.tuto.restapi.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tuto.restapi.entities.Category;
import com.tuto.restapi.services.CategoryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@CrossOrigin
@RequestMapping("/api/categories")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @GetMapping
    public ResponseEntity<List<Category>> getAllCategories(@RequestAttribute Integer userId) {
        return new ResponseEntity<>(categoryService.getCategories(userId), HttpStatus.OK);
    }

    @GetMapping("{categoryId}")
    public ResponseEntity<Category> getCategoryById(@RequestAttribute Integer userId,
            @PathVariable Integer categoryId) {
        return new ResponseEntity<>(categoryService.getCategoryById(userId, categoryId), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Category> addCategory(@RequestAttribute Integer userId, @RequestBody Category category) {
        return new ResponseEntity<>(categoryService.addCategory(userId, category), HttpStatus.CREATED);
    }

    @PutMapping("{categoryId}")
    public ResponseEntity<Category> updateCategory(@RequestAttribute Integer userId, @PathVariable Integer categoryId,
            @RequestBody Category category) {
        return new ResponseEntity<>(categoryService.updateCategory(userId, categoryId, category), HttpStatus.OK);
    }

    @DeleteMapping("{categoryId}")
    public ResponseEntity<Map<String, Boolean>> deleteCategory(@RequestAttribute Integer userId,
            @PathVariable Integer categoryId) {
        categoryService.deleteCategoryWithAllTransactions(userId, categoryId);
        Map<String, Boolean> map = new HashMap<>();
        map.put("success", true);
        return new ResponseEntity<>(map, HttpStatus.OK);
    }
}
