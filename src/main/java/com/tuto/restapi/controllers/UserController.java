package com.tuto.restapi.controllers;

import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.spec.SecretKeySpec;
import javax.validation.Valid;
import javax.xml.bind.DatatypeConverter;

import com.tuto.restapi.entities.LoginData;
import com.tuto.restapi.entities.User;
import com.tuto.restapi.services.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@RestController
@CrossOrigin
@RequestMapping("/api/users")
public class UserController {

    @Autowired
    private UserService userService;

    @Value("${JWT-Secret}")
    private String JWTTSecret;

    @Value("${JWT-Validity}")
    private long JWTTValidity;

    @PostMapping("register")
    public ResponseEntity<Map<String, String>> registerUser(@Valid @RequestBody User user) {
        userService.addUser(user);
        Map<String, String> map = new HashMap<>();
        map.put("message", "registered successfully");
        return new ResponseEntity<>(generateJWTToken(user), HttpStatus.OK);
    }

    @PostMapping("login")
    public ResponseEntity<Map<String, String>> login(@Valid @RequestBody LoginData data) {
        String email = data.getEmail();
        String password = data.getPassword();
        User user = userService.validateUser(email, password);
        Map<String, String> map = new HashMap<>();
        map.put("message", "logged successfully");
        map.put("user", user.toString());
        return new ResponseEntity<>(generateJWTToken(user), HttpStatus.OK);
    }

    private Map<String, String> generateJWTToken(User user) {
        byte[] JWTTSecretBites = DatatypeConverter.parseBase64Binary(JWTTSecret);
        Key signingKey = new SecretKeySpec(JWTTSecretBites, SignatureAlgorithm.HS256.getJcaName());
        long timestamp = System.currentTimeMillis();
        String token = Jwts.builder().signWith(SignatureAlgorithm.HS256, signingKey).setIssuedAt(new Date(timestamp))
                .setExpiration(new Date(timestamp + JWTTValidity)).claim("userId", user.getUserId())
                .claim("email", user.getEmail()).claim("firstName", user.getFirstName())
                .claim("lastName", user.getLastName()).compact();
        Map<String, String> map = new HashMap<>();
        map.put("token", token);
        return map;
    }
}
