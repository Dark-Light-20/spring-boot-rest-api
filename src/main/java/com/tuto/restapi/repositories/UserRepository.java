package com.tuto.restapi.repositories;

import com.tuto.restapi.entities.User;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    public User getByEmail(String email);

    public Boolean existsByEmail(String email);
}
