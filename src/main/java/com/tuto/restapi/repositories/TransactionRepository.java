package com.tuto.restapi.repositories;

import java.util.List;

import com.tuto.restapi.entities.Transaction;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Integer> {

    public Transaction getByUserIdAndCategoryIdAndTransactionId(Integer userId, Integer categoryId,
            Integer transactionId);

    public List<Transaction> getByUserIdAndCategoryId(Integer userId, Integer categoryId);

    public Boolean existsByUserIdAndCategoryId(Integer userId, Integer categoryId);

    public void deleteByUserIdAndCategoryIdAndTransactionId(Integer userId, Integer categoryId, Integer transactionId);

    public void deleteByUserIdAndCategoryId(Integer userId, Integer categoryId);
}
