package com.tuto.restapi.repositories;

import java.util.List;

import com.tuto.restapi.entities.Category;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer> {

    @Query(name = "getCategoryById")
    public Category getCategoryById(Integer userId, Integer categoryId);

    @Query(name = "getCategories")
    public List<Category> getCategories(Integer userId);

}
