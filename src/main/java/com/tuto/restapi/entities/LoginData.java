package com.tuto.restapi.entities;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

public class LoginData {

    @Email(message = "Email not valid")
    private String email;

    @NotBlank
    private String password;

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "{" + " email='" + getEmail() + "'" + ", password='" + getPassword() + "'" + "}";
    }

}
