package com.tuto.restapi.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "transactions")
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer transactionId;

    @NotNull
    private Integer categoryId;

    @NotNull
    private Integer userId;

    @NotNull
    private Double amount;

    @NotBlank
    private String note;

    @NotNull
    private Long transactionDate;

    public Transaction() {
    }

    public Transaction(Integer transactionId, Integer categoryId, Integer userId, Double amount, String note,
            Long transactionDate) {
        this.transactionId = transactionId;
        this.categoryId = categoryId;
        this.userId = userId;
        this.amount = amount;
        this.note = note;
        this.transactionDate = transactionDate;
    }

    public Integer getTransactionId() {
        return this.transactionId;
    }

    public void setTransactionId(Integer transactionId) {
        this.transactionId = transactionId;
    }

    public Integer getCategoryId() {
        return this.categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getUserId() {
        return this.userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Double getAmount() {
        return this.amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getNote() {
        return this.note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Long getTransactionDate() {
        return this.transactionDate;
    }

    public void setTransactionDate(Long transactionDate) {
        this.transactionDate = transactionDate;
    }

    @Override
    public String toString() {
        return "{" + " transactionId='" + getTransactionId() + "'" + ", categoryId='" + getCategoryId() + "'"
                + ", userId='" + getUserId() + "'" + ", amount='" + getAmount() + "'" + ", note='" + getNote() + "'"
                + ", transactionDate='" + getTransactionDate() + "'" + "}";
    }

}
