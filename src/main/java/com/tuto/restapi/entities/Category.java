package com.tuto.restapi.entities;

import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "categories")
@SqlResultSetMapping(name = "categoryById", classes = { @ConstructorResult(columns = {
        @ColumnResult(name = "category_id", type = Integer.class),
        @ColumnResult(name = "user_id", type = Integer.class), @ColumnResult(name = "title", type = String.class),
        @ColumnResult(name = "description", type = String.class),
        @ColumnResult(name = "total_expense", type = Double.class) }, targetClass = Category.class) })
@NamedNativeQuery(name = "getCategoryById", query = "select c.category_id, c.user_id, c.title, c.description, coalesce(sum(t.amount), 0) total_expense"
        + " from transactions t right outer join categories c on c.category_id=t.category_id "
        + " where c.user_id=?1 and c.category_id=?2", resultSetMapping = "categoryById")
@NamedNativeQuery(name = "getCategories", query = "select c.category_id, c.user_id, c.title, c.description, coalesce(sum(t.amount), 0) total_expense"
        + " from transactions t right outer join categories c on c.category_id=t.category_id "
        + " where c.user_id=?1 group by c.category_id", resultSetMapping = "categoryById")
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer categoryId;

    @NotNull
    private Integer userId;

    @NotBlank
    private String title;

    @NotBlank
    private String description;

    @Transient
    private Double totalExpense;

    public Category() {
    }

    public Category(Integer categoryId, Integer userId, String title, String description, Double totalExpense) {
        this.categoryId = categoryId;
        this.userId = userId;
        this.title = title;
        this.description = description;
        this.totalExpense = totalExpense;
    }

    public Integer getCategoryId() {
        return this.categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getUserId() {
        return this.userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getTotalExpense() {
        return this.totalExpense;
    }

    public void setTotalExpense(Double totalExpense) {
        this.totalExpense = totalExpense;
    }

    @Override
    public String toString() {
        return "{" + " categoryId='" + getCategoryId() + "'" + ", userId='" + getUserId() + "'" + ", title='"
                + getTitle() + "'" + ", description='" + getDescription() + "'" + ", totalExpense='" + getTotalExpense()
                + "'" + "}";
    }

}
